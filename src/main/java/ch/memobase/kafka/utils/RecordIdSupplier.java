/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils;

import ch.memobase.kafka.utils.models.ValueWithException;
import ch.memobase.kafka.utils.models.ValueWithKey;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.kstream.TransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

public class RecordIdSupplier<T> implements TransformerSupplier<String, T, KeyValue<String, ValueWithException<ValueWithKey<T>>>> {
    @Override
    public Transformer<String, T, KeyValue<String, ValueWithException<ValueWithKey<T>>>> get() {
        return new Transformer<String, T, KeyValue<String, ValueWithException<ValueWithKey<T>>>>() {

            ProcessorContext context;

            @Override
            public void init(ProcessorContext context) {
                this.context = context;
            }

            @Override
            public KeyValue<String, ValueWithException<ValueWithKey<T>>> transform(String key, T value) {
                String recordSetId =
                        new String(this.context
                                .headers()
                                .headers("recordSetId")
                                .iterator()
                                .next()
                                .value());
                return new KeyValue<>(recordSetId, new ValueWithException<>(new ValueWithKey<>(key, value)));
            }

            @Override
            public void close() {

            }
        };
    }
}
