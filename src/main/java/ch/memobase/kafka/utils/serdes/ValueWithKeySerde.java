/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.models.ValueWithKey;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class ValueWithKeySerde<T> implements Serde<ValueWithKey<T>> {

    private final Serde<T> valueSerde;

    public ValueWithKeySerde(Serde<T> valueSerde) {
        this.valueSerde = valueSerde;
    }

    @Override
    public Serializer<ValueWithKey<T>> serializer() {
        return new ValueWithKeySerializer<>(valueSerde.serializer());
    }

    @Override
    public Deserializer<ValueWithKey<T>> deserializer() {
        return new ValueWithKeyDeserializer<>(valueSerde.deserializer());
    }
}

