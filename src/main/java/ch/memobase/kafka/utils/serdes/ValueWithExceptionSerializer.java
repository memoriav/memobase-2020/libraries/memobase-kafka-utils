/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.ValueWithException;
import org.apache.kafka.common.serialization.Serializer;

public class ValueWithExceptionSerializer<T> implements Serializer<ValueWithException<T>> {
    private final Serializer<T> valueSerializer;
    private final Serializer<KafkaUtilsException> exceptionSerializer;
    private final static byte[] SEPARATOR = {0x01};

    public ValueWithExceptionSerializer(Serializer<T> valueSerializer, Serializer<KafkaUtilsException> exceptionSerializer) {
        this.valueSerializer = valueSerializer;
        this.exceptionSerializer = exceptionSerializer;
    }

    @Override
    public byte[] serialize(String topic, ValueWithException<T> data) {
        byte[] serializedValue;
        byte[] serializedException = data.hasException() ? this.exceptionSerializer.serialize(topic, data.getException()) : new byte[0];
        try {
            serializedValue = this.valueSerializer.serialize(topic, data.getValue());
        } catch (KafkaUtilsException ex) {
            serializedValue = new byte[0];
            if (serializedException.length == 0) {
                serializedException = this.exceptionSerializer.serialize(topic, ex);
            }
        }
        return SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, serializedValue, serializedException);
    }
}
