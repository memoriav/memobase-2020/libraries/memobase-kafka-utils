/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class SerdeUtils {
    //final static byte[] SEPARATOR = {0x00};

    /**
     * Concatenates a variable amount of byte arrays separated by separator token
     *
     * @param arrays Byte arrays to be concatenated
     * @return Concatenated byte array
     */
    protected static byte[] concatByteArraysWithSeparator(byte[] separator, byte[]... arrays) {
        int arraySize = 0;
        for (byte[] array : arrays) {
            if (array != null) {
                arraySize += 1;
            }
        }
        if (arraySize > 1) {
            int totalArrayLength = arraySize - 1;
            byte[][] filteredArray = new byte[arraySize][];
            int i = 0;
            int j = 0;
            do {
                if (arrays[i] != null) {
                    filteredArray[j] = arrays[i];
                    j++;
                }
                i++;
            } while (i < arrays.length);
            for (byte[] array : filteredArray) {
                totalArrayLength += array.length;
            }
            byte[] resultArray = new byte[totalArrayLength];
            int destArrayOffset = 0;
            for (int k = 0; k < filteredArray.length; k++) {
                if (k > 0) {
                    System.arraycopy(separator, 0, resultArray, destArrayOffset, separator.length);
                    destArrayOffset += separator.length;
                }
                System.arraycopy(filteredArray[k], 0, resultArray, destArrayOffset, filteredArray[k].length);
                destArrayOffset += filteredArray[k].length;
            }
            return resultArray;
        } else if (arraySize == 1) {
            for (byte[] array : arrays) {
                if (array != null) {
                    return array;
                }
            }
            return new byte[0];
        } else {
            return new byte[0];
        }
    }

    protected static List<byte[]> separateByteArray(byte[] separator, byte[] array) {
        int offset = 0;
        int startArrayAt = 0;
        List<byte[]> arrays = new ArrayList<>();
        while (offset < (array.length)) {
            if (array[offset] == separator[0]) {
                arrays.add(Arrays.copyOfRange(array, startArrayAt, offset));
                startArrayAt = offset + 1;
                break;
            }
            offset++;
        }
        arrays.add(Arrays.copyOfRange(array, startArrayAt, array.length));
        return arrays;
    }

    protected static boolean valueContainsSeparator(byte[] separator, byte[] value) {
        int chunkSize = separator.length;
        for (int i = 0; (i + chunkSize) <= value.length; i++) {
            if (Arrays.equals(Arrays.copyOfRange(value, i, i + chunkSize), separator)) {
                return true;
            }
        }
        return false;
    }
}
