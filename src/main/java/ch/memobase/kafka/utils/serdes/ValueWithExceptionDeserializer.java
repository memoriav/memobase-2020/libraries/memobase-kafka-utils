/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.ValueWithException;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.List;

public class ValueWithExceptionDeserializer<T> implements Deserializer<ValueWithException<T>> {
    private final Deserializer<T> valueDeserializer;
    private final Deserializer<KafkaUtilsException> exceptionDeserializer;
    private final static byte[] SEPARATOR = {0x01};

    public ValueWithExceptionDeserializer(Deserializer<T> valueDeserializer, Deserializer<KafkaUtilsException> exceptionDeserializer) {
        this.valueDeserializer = valueDeserializer;
        this.exceptionDeserializer = exceptionDeserializer;
    }

    @Override
    public ValueWithException<T> deserialize(String topic, byte[] data) {
        List<byte[]> arrays = SerdeUtils.separateByteArray(SEPARATOR, data);
        T deserializedValue;
        KafkaUtilsException deserializedException = arrays.size() > 1 && arrays.get(1).length > 0 ? this.exceptionDeserializer.deserialize(topic, arrays.get(1)) : null;
        try {
            deserializedValue = this.valueDeserializer.deserialize(topic, arrays.get(0));
        } catch (KafkaUtilsException ex) {
            deserializedValue = null;
            deserializedException = ex;
        }
        return new ValueWithException<>(deserializedValue, deserializedException);
    }
}
