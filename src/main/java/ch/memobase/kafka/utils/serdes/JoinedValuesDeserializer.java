/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.JoinedValues;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class JoinedValuesDeserializer<T, U> implements Deserializer<JoinedValues<T, U>> {
    Logger logger = LoggerFactory.getLogger(JoinedValuesDeserializer.class);
    private final Deserializer<T> leftTypeDeserializer;
    private final Deserializer<U> rightTypeDeserializer;
    private final static byte[] SEPARATOR = {0x1D};

    public JoinedValuesDeserializer(Deserializer<T> leftTypeDeserializer, Deserializer<U> rightTypeDeserializer) {
        this.leftTypeDeserializer = leftTypeDeserializer;
        this.rightTypeDeserializer = rightTypeDeserializer;
    }

    @Override
    public JoinedValues<T, U> deserialize(String topic, byte[] data) throws KafkaUtilsException {
        List<byte[]> arrays = SerdeUtils.separateByteArray(SEPARATOR, data);
        if (arrays.size() < 2) {
            throw new KafkaUtilsException("Couldn't deserialize key-value pair since separation token is missing.");
        } else if (arrays.get(0).length == 0) {
            throw new KafkaUtilsException("Left value has zero length!");
        } else if (arrays.get(1).length == 0) {
            throw new KafkaUtilsException("Right value has zero length!");
        }
        return new JoinedValues<>(
                leftTypeDeserializer.deserialize(topic, arrays.get(0)),
                rightTypeDeserializer.deserialize(topic, arrays.get(1))
        );
    }
}
