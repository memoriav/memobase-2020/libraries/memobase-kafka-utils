/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.JoinedValues;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Arrays;

public class JoinedValuesSerializer<T, U> implements Serializer<JoinedValues<T, U>> {
    private final Serializer<T> leftTypeSerializer;
    private final Serializer<U> rightTypeSerializer;
    private final static byte[] SEPARATOR = {0x1D};

    public JoinedValuesSerializer(Serializer<T> leftTypeSerializer, Serializer<U> rightTypeSerializer) {
        this.leftTypeSerializer = leftTypeSerializer;
        this.rightTypeSerializer = rightTypeSerializer;
    }

    @Override
    public byte[] serialize(String topic, JoinedValues<T, U> data) throws KafkaUtilsException {
        byte[] serializedLeftValue = this.leftTypeSerializer.serialize(topic, data.getLeft());
        byte[] serializedRightValue = this.rightTypeSerializer.serialize(topic, data.getRight());
        if (serializedLeftValue == null || serializedLeftValue.length == 0) {
            throw new KafkaUtilsException("Left value (i.e. record content) is null!");
        }
        if (serializedRightValue == null || serializedRightValue.length == 0) {
            throw new KafkaUtilsException("Right value (i.e. configuration file content) is null!");
        }
        if (SerdeUtils.valueContainsSeparator(SEPARATOR, serializedLeftValue)) {
            throw new KafkaUtilsException("Left value contains char '" + Arrays.toString(SEPARATOR) + "'. This char is used as separator token for (de-)serialization and therefore would lead to respective errors when used inside value.");
        }
        if (SerdeUtils.valueContainsSeparator(SEPARATOR, serializedRightValue)) {
            throw new KafkaUtilsException("Right value contains char '" + Arrays.toString(SEPARATOR) + "'. This char is used as separator token for (de-)serialization and therefore would lead to respective errors when used inside value.");
        }
        return SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, serializedLeftValue, serializedRightValue);
    }
}
