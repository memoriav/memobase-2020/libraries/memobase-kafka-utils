/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.models.JoinedValues;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class JoinedValuesSerde<T, U> implements Serde<JoinedValues<T, U>> {
    Serde<T> leftValueSerde;
    Serde<U> rightValueSerde;

    public JoinedValuesSerde(Serde<T> leftValueSerde, Serde<U> rightValueSerde) {
        this.leftValueSerde = leftValueSerde;
        this.rightValueSerde = rightValueSerde;
    }

    @Override
    public Serializer<JoinedValues<T, U>> serializer() {
        return new JoinedValuesSerializer<>(leftValueSerde.serializer(), rightValueSerde.serializer());
    }

    @Override
    public Deserializer<JoinedValues<T, U>> deserializer() {
        return new JoinedValuesDeserializer<>(leftValueSerde.deserializer(), rightValueSerde.deserializer());
    }

}

