/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.ValueWithKey;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.List;

public class ValueWithKeyDeserializer<T> implements Deserializer<ValueWithKey<T>> {
    private final Deserializer<T> valueDeserializer;
    private final static byte[] SEPARATOR = {0x00};

    public ValueWithKeyDeserializer(Deserializer<T> valueDeserializer) {
        this.valueDeserializer = valueDeserializer;
    }

    @Override
    public ValueWithKey<T> deserialize(String topic, byte[] data) throws KafkaUtilsException {
        List<byte[]> arrays = SerdeUtils.separateByteArray(SEPARATOR, data);
        if (arrays.size() < 2) {
            throw new KafkaUtilsException("Couldn't deserialize key-value pair since separation token is missing");
        } else if (arrays.get(0).length == 0) {
            throw new KafkaUtilsException("Key has zero length!");
        } else if (arrays.get(1).length == 0) {
            throw new KafkaUtilsException("Value has zero length!");
        }
        return new ValueWithKey<>(
                new String(arrays.get(1)),
                valueDeserializer.deserialize(topic, arrays.get(0))
        );
    }
}
