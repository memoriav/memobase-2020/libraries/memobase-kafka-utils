/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.models;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;

public class ValueWithException<T> {
    private T value;
    private KafkaUtilsException exception;

    public ValueWithException(T value) {
        this.value = value;
        this.exception = null;
    }

    public ValueWithException(T value, KafkaUtilsException exception) {
        this.value = value;
        this.exception = exception;
    }

    public boolean hasException() {
        return exception != null;
    }

    public boolean hasValue() {
        return value != null;
    }

    public KafkaUtilsException getException() {
        return this.exception;
    }

    public void setException(KafkaUtilsException exception) {
        this.exception = exception;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

}
