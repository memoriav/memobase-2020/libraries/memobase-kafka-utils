/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.models;

/**
 * Helper class to represent two values matched by key. The idea is to later merge these two values.
 *
 * @param <T> Type of left value
 * @param <U> Type of right value
 */
public class JoinedValues<T, U> {
    private final T left;
    private final U right;

    public JoinedValues(T left, U right) {
        this.left = left;
        this.right = right;
    }

    public T getLeft() {
        return left;
    }

    public boolean hasLeft() {
        return left != null;
    }

    public U getRight() {
        return right;
    }

    public boolean hasRight() {
        return right != null;
    }
}
