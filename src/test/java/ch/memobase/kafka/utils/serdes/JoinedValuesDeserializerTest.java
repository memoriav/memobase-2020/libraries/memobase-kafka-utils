/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.JoinedValues;
import ch.memobase.kafka.utils.models.ValueWithKey;
import org.apache.kafka.common.serialization.Serdes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JoinedValuesDeserializerTest {

    @Test
    void deserialize() {
        byte[] leftValue = "!".getBytes();
        byte[] value = {'h', 'e', 'l', 'l', 'o'};
        ValueWithKey<byte[]> valueWithKey = new ValueWithKey<>("world", value);
        ValueWithKeySerializer<byte[]> valueWithKeySerializer = new ValueWithKeySerializer<>(Serdes.ByteArray().serializer());
        byte[] valueWithKeySerialized = valueWithKeySerializer.serialize("test-topic", valueWithKey);

        byte[] serializedObject = SerdeUtils.concatByteArraysWithSeparator(new byte[]{0x1D}, leftValue, valueWithKeySerialized);

        JoinedValuesDeserializer<String, ValueWithKey<byte[]>> deserializer =
                new JoinedValuesDeserializer<>(Serdes.String().deserializer(), new ValueWithKeyDeserializer<>(Serdes.ByteArray().deserializer()));

        JoinedValues<String, ValueWithKey<byte[]>> res = deserializer.deserialize("test-topic", serializedObject);

        assertEquals("!", res.getLeft());
        assertEquals("world", res.getRight().getKey());
        assertArrayEquals(value, res.getRight().getValue());

    }

    @Test
    void deserializeGarbageData() {
        JoinedValuesDeserializer<String, String> deserializer =
                new JoinedValuesDeserializer<>(Serdes.String().deserializer(), Serdes.String().deserializer());

        byte[] garbageData = {0x00, 0x35, 0x23, 0x53, 0x43};
        byte[] garbageData2 = {0x44, 0x35, 0x23, 0x53, 0x00};
        assertThrows(KafkaUtilsException.class, () -> deserializer.deserialize("aTopic", garbageData));
        assertThrows(KafkaUtilsException.class, () -> deserializer.deserialize("aTopic", garbageData2));

    }
}