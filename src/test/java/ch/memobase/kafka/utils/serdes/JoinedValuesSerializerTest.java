/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.JoinedValues;
import ch.memobase.kafka.utils.models.ValueWithKey;
import org.apache.kafka.common.serialization.Serdes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JoinedValuesSerializerTest {

    @Test
    void serialize() {
        String leftValue = "leftValue";
        String rightValueKey = "rightValueKey";
        String rightValueValue = "rightValueValue";
        JoinedValues<String, ValueWithKey<String>> testObj = new JoinedValues<>(leftValue, new ValueWithKey<>(rightValueKey, rightValueValue));
        JoinedValuesSerializer<String, ValueWithKey<String>> serializer = new JoinedValuesSerializer<>(Serdes.String().serializer(), new ValueWithKeySerializer<>(Serdes.String().serializer()));
        JoinedValuesDeserializer<String, ValueWithKey<String>> deserializer = new JoinedValuesDeserializer<>(Serdes.String().deserializer(), new ValueWithKeyDeserializer<>(Serdes.String().deserializer()));
        byte[] intermediaryRes = serializer.serialize("test-topic", testObj);
        JoinedValues<String, ValueWithKey<String>> res = deserializer.deserialize("test-topic", intermediaryRes);

        assertEquals(leftValue, res.getLeft());
        assertEquals(rightValueKey, res.getRight().getKey());
        assertEquals(rightValueValue, res.getRight().getValue());
    }

    @Test
    void serializeGarbageData() {
        JoinedValuesSerializer<String, String> serializer = new JoinedValuesSerializer<>(Serdes.String().serializer(), Serdes.String().serializer());

        JoinedValues<String, String> garbageData1 = new JoinedValues<>("", "world");
        JoinedValues<String, String> garbageData2 = new JoinedValues<>("hello", "");
        JoinedValues<String, String> garbageData3 = new JoinedValues<>(null, "world");
        JoinedValues<String, String> garbageData4 = new JoinedValues<>("hello", null);


        assertThrows(KafkaUtilsException.class, () -> serializer.serialize("aTopic", garbageData1));
        assertThrows(KafkaUtilsException.class, () -> serializer.serialize("aTopic", garbageData2));
        assertThrows(KafkaUtilsException.class, () -> serializer.serialize("aTopic", garbageData3));
        assertThrows(KafkaUtilsException.class, () -> serializer.serialize("aTopic", garbageData4));

    }
}