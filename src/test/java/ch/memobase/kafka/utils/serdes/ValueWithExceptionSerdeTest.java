/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import ch.memobase.kafka.utils.errors.KafkaUtilsException;
import ch.memobase.kafka.utils.models.ValueWithException;
import ch.memobase.kafka.utils.models.ValueWithKey;
import org.apache.kafka.common.serialization.Serdes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValueWithExceptionSerdeTest {

    @Test
    void serializeObjectWithoutException() {
        ValueWithException<ValueWithKey<String>> valueWithException = new ValueWithException<>(new ValueWithKey<>("hello", "world"));
        ValueWithKeySerde<String> valueWithKeySerde = new ValueWithKeySerde<>(Serdes.String());
        ValueWithExceptionSerde<ValueWithKey<String>> valueWithExceptionSerde = new ValueWithExceptionSerde<>(valueWithKeySerde);

        byte[] serializedValueWithException = valueWithExceptionSerde.serializer().serialize("aTopic", valueWithException);
        ValueWithException<ValueWithKey<String>> deserializedValueWithException = valueWithExceptionSerde.deserializer().deserialize("aTopic", serializedValueWithException);
        assertSame(valueWithException.getException(), deserializedValueWithException.getException());
        assertEquals(deserializedValueWithException.getValue().getKey(), valueWithException.getValue().getKey());
        assertEquals(deserializedValueWithException.getValue().getValue(), valueWithException.getValue().getValue());
    }

    @Test
    void serializeObjectWithException() {
        ValueWithException<ValueWithKey<String>> valueWithException = new ValueWithException<>(new ValueWithKey<>("hello", "world"), new KafkaUtilsException("Oh no!"));
        ValueWithKeySerde<String> valueWithKeySerde = new ValueWithKeySerde<>(Serdes.String());
        ValueWithExceptionSerde<ValueWithKey<String>> valueWithExceptionSerde = new ValueWithExceptionSerde<>(valueWithKeySerde);

        byte[] serializedValueWithException = valueWithExceptionSerde.serializer().serialize("aTopic", valueWithException);
        ValueWithException<ValueWithKey<String>> deserializedValueWithException = valueWithExceptionSerde.deserializer().deserialize("aTopic", serializedValueWithException);
        assertNotNull(deserializedValueWithException.getException());
        assertEquals(deserializedValueWithException.getValue().getKey(), valueWithException.getValue().getKey());
        assertEquals(deserializedValueWithException.getValue().getValue(), valueWithException.getValue().getValue());
    }

    @Test
    void serializeObjectWhichThrowsSerializationException() {
        ValueWithException<ValueWithKey<String>> valueWithException = new ValueWithException<>(new ValueWithKey<>("hello", ""));
        ValueWithKeySerde<String> valueWithKeySerde = new ValueWithKeySerde<>(Serdes.String());
        ValueWithExceptionSerde<ValueWithKey<String>> valueWithExceptionSerde = new ValueWithExceptionSerde<>(valueWithKeySerde);

        byte[] serializedValueWithException = valueWithExceptionSerde.serializer().serialize("aTopic", valueWithException);
        ValueWithException<ValueWithKey<String>> deserializedValueWithException = valueWithExceptionSerde.deserializer().deserialize("aTopic", serializedValueWithException);
        assertNotNull(deserializedValueWithException.getException());
        assertFalse(deserializedValueWithException.hasValue());
    }
}