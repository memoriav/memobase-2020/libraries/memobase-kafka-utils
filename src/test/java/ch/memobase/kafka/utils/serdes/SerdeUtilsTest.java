/*
 * Copyright 2020 Memoriav
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.memobase.kafka.utils.serdes;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SerdeUtilsTest {

    byte[] helloWorld = {'h', 'e', 'l', 'l', 'o', 0x00, 'w', 'o', 'r', 'l', 'd'};
    byte[] hello = {'h', 'e', 'l', 'l', 'o'};
    byte[] emptyHello = {0x00, 'h', 'e', 'l', 'l', 'o'};
    byte[] helloEmpty = {'h', 'e', 'l', 'l', 'o', 0x00};
    byte[] world = {'w', 'o', 'r', 'l', 'd'};
    private final static byte[] SEPARATOR = {0x00};

    @Test
    void concatByteArraysWithSeparatorThreeValues() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, hello, null, world);
        assertArrayEquals(helloWorld, result);
    }

    @Test
    void concatTwoZeroLengthByteArrays() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, new byte[0], new byte[0]);
        assertArrayEquals(SEPARATOR, result);
    }

    @Test
    void concatZeroLengthByteArrayWithNonZeroLengthByteArray() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, new byte[0], hello);
        assertArrayEquals(emptyHello, result);
    }

    @Test
    void concatNonZeroLengthByteArrayWithZeroLengthByteArray() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, hello, new byte[0]);
        assertArrayEquals(helloEmpty, result);
    }

    @Test
    void concatByteArraysWithSeparatorTwoValues() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, hello, world);
        assertArrayEquals(helloWorld, result);
    }

    @Test
    void concatByteArraysWithSeparatorOneValue() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, hello);
        assertArrayEquals(hello, result);
    }

    @Test
    void concatByteArraysWithSeparatorNullValues() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR);
        assertArrayEquals(new byte[0], result);
    }

    @Test
    void concatByteArraysWithSeparatorNullValue() {
        byte[] result = SerdeUtils.concatByteArraysWithSeparator(SEPARATOR, hello);
        assertArrayEquals(hello, result);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void separateNullValue() {
        assertThrows(NullPointerException.class, () -> SerdeUtils.separateByteArray(SEPARATOR, null));
    }

    @Test
    void separateZeroLengthValue() {
        List<byte[]> separated = SerdeUtils.separateByteArray(SEPARATOR, new byte[0]);
        assertArrayEquals(new byte[0], separated.get(0));
    }

    @Test
    void separateOneValue() {
        List<byte[]> separated = SerdeUtils.separateByteArray(SEPARATOR, hello);
        assertArrayEquals(hello, separated.get(0));
        assert (separated.size() == 1);
    }

    @Test
    void separateTwoByteArrays() {
        List<byte[]> separated = SerdeUtils.separateByteArray(SEPARATOR, helloWorld);
        assertArrayEquals(hello, separated.get(0));
        assertArrayEquals(world, separated.get(1));
    }

    @Test
    void valueContainsSeparator() {
        assertTrue(SerdeUtils.valueContainsSeparator(new byte[]{'l', 'l'}, helloEmpty));
        assertTrue(SerdeUtils.valueContainsSeparator(new byte[]{'o', 0x00}, helloEmpty));
        assertTrue(SerdeUtils.valueContainsSeparator(new byte[]{'h', 'e'}, helloEmpty));
    }

    @Test
    void valueNotContainsSeparator() {
        assertFalse(SerdeUtils.valueContainsSeparator(new byte[]{'o', 'l'}, helloEmpty));
    }
}