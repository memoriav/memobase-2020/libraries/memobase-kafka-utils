# Memobase Kafka Utils

Releases: [Package registry](https://gitlab.switch.ch/memoriav/memobase-2020/libraries/package-registry/-/packages)

A collection of util classes to support services who interacts with the Memobase Kafka cluster.

### Used By

- [XML Data Transform](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/xml-data-transform)
- [Mapper Service](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/mapper-service)
- [Normalization Service](https://gitlab.switch.ch/memoriav/memobase-2020/services/import-process/normalization-service)

## Config Join

Certain services in the Memobase import process need additional, per-collection configuration which is not provided in the header of each message. These configurations are sent in a dedicated Kafka topic and consumed by the service. The `ConfigJoiner` and its `join` method helps the service to bring together a document with its respective configuration. For this purpose, it builds a lookup table with all available configurations (internally represented as a `KTable`). Once a document arrives at the service, the `join` method is used to perform the lookup. A match is then wrapped in an instance of `ValueWithException`, a class who represents either the document-configuration pair or an exception if something went wrong.
