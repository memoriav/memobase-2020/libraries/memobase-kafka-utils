plugins {
    `maven-publish`
    `java-library`
    id("io.freefair.git-version") version "6.2.0"
    jacoco
    id("org.jetbrains.dokka") version "1.9.20"
}

group = "ch.memobase"

val log4jV = "2.23.1"
val kafkaV = "3.8.0"


repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}


dependencies {
    implementation("org.apache.kafka:kafka-clients:${kafkaV}")
    implementation("org.apache.kafka:kafka-streams:${kafkaV}")
    implementation("org.apache.logging.log4j:log4j-api:${log4jV}")
    implementation("org.apache.logging.log4j:log4j-core:${log4jV}")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:${log4jV}")

    testImplementation("org.junit.jupiter:junit-jupiter:5.11.0")
    testImplementation("org.apache.kafka:kafka-streams-test-utils:${kafkaV}")
}

configurations {
    all {
        exclude(group = "org.slf4j", module = "slf4j-log4j12")
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()
    testLogging {
        setEvents(mutableListOf("passed", "skipped", "failed"))
    }
}

/* tasks.javadoc {
    destinationDir = file("public/")
} */

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        csv.required = true // html reports are the default, so not explicitly enabled here
    }
}

publishing {
    publications {
        create<MavenPublication>("memobase-kafka-utils") {
            groupId = "ch.memobase"
            artifactId = "memobase-kafka-utils"
            from(components["java"])
            pom {
                name = "Memobase Kafka Utils"
                description = "Utils library for Kafka sevices"
                url = "https://gitlab.switch.ch/memoriav/memobase-2020/libraries/memobase-kafka-utils"
                licenses {
                    license {
                        name = "The Apache License, Version 2.0"
                        url = "https://www.apache.org/licenses/LICENSE-2.0.txt"
                    }

                }
                developers {
                    developer {
                        id = "sschuepbach"
                        name = "Sebastian Schüpbach"
                        email = "sebastian.schuepbach@unibas.ch"
                    }
                }
                scm {
                    connection =
                        "scm:git:https://gitlab.switch.ch/memoriav/memobase-2020/libraries/memobase-kafka-utils.git"
                    developerConnection =
                        "scm:git:git@gitlab.switch.ch:memoriav/memobase-2020/libraries/memobase-kafka-utils.git"
                    url = "https://gitlab.switch.ch/memoriav/memobase-2020/libraries/memobase-kafka-utils"
                }

            }
        }

        repositories {
            maven {
                url = uri("https://gitlab.switch.ch/api/v4/projects/1324/packages/maven")
                name = "GitLabRepository"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }
    }
}
